3-mavzu

PROPS

Props - React componentlariga uzatiladigan parameterlardir
Props componentlarga HTML attributlar yordamida uzatiladi
Misol:
const mashina = <Mashina brand="Malibu" />;

Component tepada uzatilgan ma'lumotni props objecti sifatida qabul qiladi
Misol:
function Mashina(props) {
  return <h2>Mashina brandi { props.brand }!</h2>;
}

Componentga propsni ota componentidan yuvorishimiz ham mumkin
function Mashina(props) {
  return <h2>Mashinaning brandi{ props.brand }!</h2>;
}

function Garaj() {
  return (
    <>
      <h1>Garajda nima mashina bor?</h1>
      <Mashina brand="Malibu" />
    </>
  );
}

ReactDOM.render(<Garaj />, document.getElementById('root'));

-------------------------------------------
O'zgaruvchini props sifatida yuvorish uchun {} ichida yozing
const mashinaBrandi = 'Malibu';
const mashina = <Mashina brand={mashinaBrandi} />;

Topshiriq: Salomlashish componentini tuzing va unga props yordamida
salomlashayotgan ismni qiymatini yuboring

Topshiriq: ToDo classidan List classiga qilinadigan ishlarning arrayini
yuboring
const List = (props) => {
  { /* Change code below this line */ }
  return <p></p>
  { /* Change code above this line */ }
};

class ToDo extends React.Component {
  constructor(props) {
    super(props);
  }
  render() {
    return (
      <div>
        <h1>To Do Lists</h1>
        <h2>Today</h2>
        { /* Change code below this line */ }
        <List />
        <h2>Tomorrow</h2>
        <List />
        { /* Change code above this line */ }
      </div>
    );
  }
};

Topshiriq: https://getbootstrap.com/docs/4.0/examples/pricing/ ni yasash
Props ni o'zgartirib bulmaydi.
-------------------------------------------
defaultProps - bu agar props berilmasa Componentning propslari qanday qiymat olishini
belgilash yo'lidir
myComponent.defaultProps = {
  ism: 'Eshmat'
}
============================================
EVENTS
Eventslarni camelCase da yozamiz: 
onclick ning o'rniga onClick.

Event chaqirayotgan function {} orasida yoziladi:
onclick="myFunction()" o'rniga onClick={myFunction} yozing 

Event chaqirayotgan functionga parameter uzatish kerak bo'lsa quyidagi yo'l bn uzatiladi:
function Football() {
  const shoot = (a) => {
    alert(a);
  }

  return (
    <button onClick={() => shoot("Goal!")}>Take the shot!</button>
  );
}

Tepadagi holatda () ichida biz event parameterni qabul qilishimiz mumkin
const shoot = (a, event) => {
    alert(event.type);
}
<button onClick={(event) => shoot("Goal!", event)}>Take the shot!</button>


===========================================
LIST
Reactda arraylarni map() funksiyasi orqali ekranga chiqaramiz
Misol:
function Mashina(props) {
  return <li>I am a { props.brand }</li>;
}

function Garaj() {
  const mashinalar = ['Ford', 'BMW', 'Audi'];
  return (
    <>
      <h1>Garajda quyidagi mashinalar mavjud</h1>
      <ul>
        {mashinalar.map((mashina) => <Mashina brand={mashina} />)}
      </ul>
    </>
  );
}

ReactDOM.render(<Garaj />, document.getElementById('root'));


Ammo bu kodda bizda "warning" yuzaga keladi. Chunki biz har bir <li> uchun key 
berishimiz kerak.
Key Reactga elementlarni kuzatish imkonini beradi. Agar biror <li> yangilangan 
yoki o'chirilgan bo'lsa, butun <ul> yoki <ol> emas balki faqat o'sha element qayta 
ko'rsatiladi.
Key har bir ro'yxatdagi <li> uchun yagona bo'lishi kerak

Tepedagi misolni key bilan yozish quyidagicha buladi:
function Mashina(props) {
  return <li>I am a { props.brand }</li>;
}

function Garaj() {
  const mashinalar = [
    {id: 1, brand: 'Ford'},
    {id: 2, brand: 'BMW'},
    {id: 3, brand: 'Audi'}
  ];
  return (
    <>
      <h1>Garajda quyidagi mashinalar mavjud</h1>
      <ul>
        {mashinalar.map((mashina) => <Mashina key={mashina.id} brand={mashina} />)}
      </ul>
    </>
  );
}

ReactDOM.render(<Garaj />, document.getElementById('root'));
========================================
IF NI ISHLATISH

import React from 'react';
import ReactDOM from 'react-dom';

Misol:
function MissedGoal() {
	return <h1>MISSED!</h1>;
}

function MadeGoal() {
	return <h1>GOAL!</h1>;
}

function Goal(props) {
  const isGoal = props.isGoal;
  if (isGoal) { // agar gol bulsa MadeGoal ni yuklash
    return <MadeGoal/>;
  }
  return <MissedGoal/>; // aks holda MissedGoal ni yuklash
}
------------------
return (
    <>
      { isGoal ? <MadeGoal/> : <MissedGoal/> }
    </>
------------------
ReactDOM.render(
  <Goal isGoal={true} />,
  document.getElementById('root')
);

